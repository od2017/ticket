
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Counter extends JFrame {

	private JPanel contentPane;

	
	/**
	 * Create the frame.
	 */
	public Counter() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		setBounds(10, 10, 450, 300);
		contentPane = new JPanel();
		//contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		contentPane.setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		
		JLabel lb1 =new JLabel("You get client Number.. ");
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.PAGE_START;
		
		contentPane.add(lb1, c);
		
		JButton btn =new JButton("Next>");
		btn.setPreferredSize(new Dimension(22,55));
		//c.fill = GridBagConstraints.PAGE_END;
		c.anchor = GridBagConstraints.PAGE_END;
		
		c.gridx = 1;
		c.gridy = 0;
		
		contentPane.add(btn, c);
		
		JButton btn2 =new JButton("Recall>");
		btn2.setPreferredSize(new Dimension(22,55));
		//c.fill = GridBagConstraints.BOTH;
		
		c.gridx = 1;
		c.gridy = 1;
		contentPane.add(btn2, c);
		
		
		
		JLabel lb =new JLabel("445");
		lb.setFont(new Font("Serif", Font.PLAIN, 66));
		lb.setHorizontalAlignment(JLabel.CENTER);
		
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 1;
		contentPane.add(lb, c);
			
		
		
	
	}

}
